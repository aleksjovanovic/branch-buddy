#!/bin/bash

URL="localhost"
PORT="8080"

curl --request GET \
  --user admin:admin \
  --url "http://${URL}:${PORT}/rest/agile/1.0/board" \
  --header 'Accept: application/json'
