package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/manifoldco/promptui"
	//"gopkg.in/yaml.v3"
)

// TODO: Read config from yaml ./config/config.yaml
const HOST = "localhost"
const PORT = "8080"
const TOKEN = "NTYwMjU2NTM2NTcxOvXKrOXT+T8PZCGvkLyPOADgUpfX"
const USERNAME = "admin"
const PASSWORD = USERNAME
const BOARD_ID = "1"

func main() {
	prompt := promptui.Select{
		Label: "Action",
		Items: []string{"Get Sprint", "Exit"},
	}

	_, result, err := prompt.Run()

	if err != nil {
		fmt.Printf("Prompt failed %v\n", err)
		return
	}

	switch result {
	case "Setup":
		setup()
	case "Get Sprint":
		getSprint()
	case "Exit":
		fmt.Printf("Bye <3")
		os.Exit(0)
	default:
		fmt.Printf("I don't understand what '%s' means...", result)
	}
}

func setup() {
	fmt.Printf("Not Implemented Yet")
	os.Exit(2)
}

func getSprint() {
	var url = getBaseUrl() + "/rest/agile/1.0/board/" + BOARD_ID + "/sprint"
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	req.SetBasicAuth("admin", "admin")
	resp, err := client.Do(req)
	//resp, err := http.Get(sprints)
	if err != nil {
		fmt.Printf("Loading sprints has failed")
		os.Exit(5)
	}
	// io.ReadCloser to String
	buf := new(strings.Builder)
	io.Copy(buf, resp.Body)
	fmt.Printf("Gettints sprints: " + buf.String())

	// Turn the buffer into an object
}

func getBaseUrl() string {
	return "http://" + HOST + ":" + PORT
}
